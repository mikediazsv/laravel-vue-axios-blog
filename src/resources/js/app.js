import './bootstrap';
//importamos vue

/*
//ESTA OPCION DE CODIGO ME FUNCIONÓ PARA VUE3 PERO LAS PAGINAS NO CARGABAN BIEN, NO ENCONTRE SOLUCION, POR ESO ME CAMBIE A @VUE2.6.14
import {createApp} from 'vue';

require('./bootstrap');


import App from './components/App.vue';
import axios from 'axios';
import { routes } from './routes';

const app = createApp(App);
app.config.globalProperties.$axios = axios;
app.use(routes);
app.mount('#app');*/

/*Este codigo funciona con la version vue2.6.14 y no da el error "export 'default' (imported as 'vue') was not found in 'vue'"*/ 
import * as vue from 'vue'

//import vue from 'vue'
//window.vue = vue;
window.Vue = require('vue').default;
import App from './components/App.vue';

//importamos Axios
import VueAxios from 'vue-axios';
import axios from 'axios';

//Importamos y configuramos el Vue-router
import VueRouter from 'vue-router';
import {routes} from './routes';
//import vue from 'vue';
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
 
const router = new VueRouter({
    mode: 'history',
    routes: routes
});
 
//finalmente, definimos nuestra app de Vue
const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(App),
});