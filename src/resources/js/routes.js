

/**
 * primero importamos los componentes que acabamos de crear
   App.vue no lo llamamos aqui, sino que lo vamos a referencias desde otro archivo
 */
   const Home = () => import('./components/Home.vue')
   const Contacto = () => import('./components/Contacto.vue')

 //importamos los componentes para el Blog
 const Mostrar = () => import('./components/blog/Mostrar.vue')
const Crear = () => import('./components/blog/Crear.vue')
const Editar = () => import('./components/blog/Editar.vue')

 //vamos a crear una constante de tipo route


 export const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'mostrarBlogs',
        path: '/blogs',
        component: Mostrar
    },
    {
        name: 'crearBlog',
        path: '/crear',
        component: Crear
    },
    {
        name: 'editarBlog',
        path: '/editar/:id',
        component: Editar
    },
     {
        name: 'contacto',
        path: '/contacto',
        component: Contacto
    }
]